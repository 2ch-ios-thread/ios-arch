import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    let navigationController = UINavigationController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        guard let window = window else {
            return false
        }
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        // Здесь на `navigationController` пушим ThreadScreen
        
        return true
    }
}
