import Foundation

class NetworkingService {
    
    func getPosts(completion: @escaping ([Post]) -> Void) {
        let threadNumber = "1279094"
        let url = URL(string: "https://2ch.hk/makaba/mobile.fcgi?task=get_thread&board=pr&thread=\(threadNumber)&post=0")!
        
        URLSession.shared.dataTask(with: url) { data, _, _ in
            let posts = try! JSONDecoder().decode([Post].self, from: data!)
            DispatchQueue.main.async {
                completion(posts)
            }
        }.resume()
    }
    
    func getPostDetails(by num: String, completion: @escaping (PostDetails) -> Void) {
        let url = URL(string: "https://2ch.hk/makaba/mobile.fcgi?task=get_post&board=pr&post=\(num)")!
        
        URLSession.shared.dataTask(with: url) { data, _, _ in
            let posts = try! JSONDecoder().decode([PostDetails].self, from: data!)
            DispatchQueue.main.async {
                completion(posts.first!)
            }
        }.resume()
    }
}

struct Post: Decodable {
    let comment: String
    let date: String
    let num: String
}

struct PostDetails: Decodable {
    let name: String
    let email: String
}
